/**
* /models/message.js
*/

//import bibl `mongoose`
var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');

//tworzenie schematu
var Schema = mongoose.Schema;

var schema = Schema({
	//poleDB: {type: xxx, opt: xxx}
	content: {type: String, required: true},
	user: {type: Schema.Types.ObjectId, ref: 'User'}
});

schema.plugin(timestamps,  {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});

module.exports = 
	mongoose.model('Message', schema);