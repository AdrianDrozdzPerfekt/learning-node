/**
* /models/user.js
*/

var mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');
var uniqueValidator = require('mongoose-unique-validator');
var passportLocalMongoose = require('passport-local-mongoose');
var bcrypt = require('bcryptjs');

var Schema = mongoose.Schema;

var userSchema = Schema({
	name: {type: String, required: true},
	email: {type: String, required: true, unique: true},
	password: {type: String, required: true},
	messages: [{type: Schema.Types.ObjectId, ref: 'Message'}]
});

/**generowanie zaszyfrowanego hasla - podczas rejestracji*/
userSchema.methods.generateHash = function(password) {
	return bcrypt.hashSync(password, 10);
}

/**sprawdzanie poprawnosci hasla - podczas logowania*/
userSchema.methods.validatePassword = function(password) {
	return bcrypt.compareSync(password, this.password)
}



userSchema.plugin(uniqueValidator);
userSchema.plugin(passportLocalMongoose, {
	usernameField: 'email',
	usernameQueryFields: ['email']
});
/**
mongo 
use node-app
db.dropDatabase()
restart apki
*/
userSchema.plugin(timestamps,  {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
});

module.exports = mongoose.model('User', userSchema);