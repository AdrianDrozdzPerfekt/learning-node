//node szuka w swoich paczkach
//modulu o nazwie http
var http = require('http');
//modul czytajacy plki
var fs = require('fs');

//utworz serwer
http.createServer(function(req, res) { //callback function

	if (req.url === '/') {
		fs.createReadStream(__dirname 
			+ '/index.html').pipe(res);
	} else if (req.url === '/api') {
		
		res.writeHead(200, { 
			'Content-Type': 'application/json' 
		});

		var object = {
			firstname: 'John',
			lastname: 'Doe'
		};

		res.end(JSON.stringify(object));
	} else {
		res.writeHead(404);
		res.end();
	}

}).listen(1337, '127.0.0.1'); //localhost:1337

