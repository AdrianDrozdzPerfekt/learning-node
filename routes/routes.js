var bodyParser 			= require('body-parser');
var urlencodedParser 	= bodyParser.urlencoded({ 
	extended: false 
});

var User 				= require('../models/user');
var Message 			= require('../models/message');


//Eksport tras do appe.js do pozniejszego importu
module.exports = function(app, passport) {
	//homepage
	app.get('/', function(request, response){
		Message.find()
			.sort({updated_at: 'desc'}) //sortowanie malejace
			.populate('user')
			.exec(function(err, msgs) {
				if (err)
					throw err;
				
				response.render('index', {
					messages: msgs,
					noteId: request.query.note
				})
			})
	});

	//register page
	app.get('/rejestracja', function(req, res) {
		res.render('rejestracja');
	});

	//POST register
	app.post('/rejestracja', passport.authenticate('local-signup', {
		failureRedirect: '/rejestracja',
		successRedirect : '/',
		failureFlash: true,
	}));
	
	//POST login
	app.post('/logowanie', passport.authenticate('local-login', {
		failureRedirect: '/rejestracja',
		successRedirect : '/',
		failureFlash: true,
	}));

	app.get('/wyloguj', function(req, res) {
		req.logout();
		res.redirect('/');
	})


	//obsluga metody GET / - str glowna
	app.get('/admin', function(request, response){
		User.find().sort({updated_at: 'desc'}).exec(function(err, users){
			if (err) {
				console.log(err);
				return response.send('Error');
			}
			//PO POPRAWNYM POBRANIU DANYCH Z DB
			//wyrendreowanie strony BEZ ROZSZERZENIA
			response.render('admin', {
				users: users,
				pageTitle: 'Users'
			});
		});
		/**!!! W TYM MIEJSCU NIE MOZE BYC NIC
			BO ZAPYTANIE DO DB JEST ASYNCHRONICZNE
			I MUSIMY POCZEKAC NA ZWROT DANYCH Z DB*/
	});

	//POST /save-messeage	
	app.post('/save-message', isLoggedIn, function(req, res) {
		var message = new Message();
		//przypisanie tresci z forma
		message.content = req.body.noteContent;
		//przypisanie zalogowanego usera
		message.user = req.user;

		message.save(function(err, msg) {
			if (err) {
				//ustawiamy flash
				req.flash('danger', 'Nie dodano notatki');
				//powrot na SG z przerwaniem skryptu
				return res.redirect('/');
			}

			//dopisujemy notatke do usera
			req.user.messages.push(msg);
			req.user.save();

			//ustawiamy flash
			req.flash('success', 'Pomyślnie dodano notatkę');
			//powrot na SG
			res.redirect('/');

		});
	});

	//DELETE usuwanie notatki o danym ID
	app.delete('/delete-message/:id', isLoggedIn, function (req, res) {
		Message.findById(req.params.id)
			.populate('user')
			.exec(function (err, message) {
				if (err) {
					return res.status(500).send('Message not found');
				}

				if (!message) {
					return res.status(500).send('Message not found');	
				}
				
				if (req.user.id !== message.user.id) {
					return res.status(403).send('Message not editable');		
				}

				message.remove(function (err, result) {
					if (err) {
						return res.status(500).send('Message not found');		
					}

					return res.status(200).send('Message deleted');
				});

			});
	})

}

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) //is users logged in = authenticated
		return next();

	res.redirect('/');
}