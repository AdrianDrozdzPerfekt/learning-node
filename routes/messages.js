var express = require('express');
var router = express.Router();

var Message = require('../models/message');

//pobranie notatek
router.get('/', function (req, res, next) {
	return res.send('all notes')
});
//dodanie notatki
router.post('/', function (req, res, next) {
	return res.send('add note')
});
//get note
router.get('/:id', isLoggedIn, function (req, res, next) {
	Message.findById(req.params.id)
			.populate('user')
			.exec(function(err, msg) {
				if (err) {
					//ustawiamy flash
					req.flash('danger', 'Nieoczekiwany błąd');
					//powrot na SG z przerwaniem skryptu
					return res.redirect('/');
				}


				if (!msg) {
					//ustawiamy flash
					req.flash('danger', 'Nie ma takiej notatki');
					//powrot na SG z przerwaniem skryptu
					return res.redirect('/');
				}

				if (req.user.id != msg.user.id) {
					//ustawiamy flash
					req.flash('danger', 'Nie jesteś właścicielem');
					//powrot na SG z przerwaniem skryptu
					return res.redirect('/');
				}

				return res.render('message/update', {
					message: msg
				});


			})
});
//update contentu notatki
router.post('/:id', function (req, res, next) {
	Message.findById(req.body.id)
			.populate('user')
			.exec(function(err, msg) {
				if (err) {
					//ustawiamy flash
					req.flash('danger', 'Nieoczekiwany błąd');
					//powrot na SG z przerwaniem skryptu
					return res.redirect('/');
				}


				if (!msg) {
					//ustawiamy flash
					req.flash('danger', 'Nie ma takiej notatki');
					//powrot na SG z przerwaniem skryptu
					return res.redirect('/');
				}

				if (req.user.id != msg.user.id) {
					//ustawiamy flash
					req.flash('danger', 'Nie jesteś właścicielem');
					//powrot na SG z przerwaniem skryptu
					return res.redirect('/');
				}

				msg.content = req.body.content;
				msg.save(function(err, updatedMessage) {
					if (err) {
						//ustawiamy flash
						req.flash('danger', 'Nieoczekiwany błąd');
						//powrot na SG z przerwaniem skryptu
						return res.redirect('/');
					}

					//ustawiamy flash
					req.flash('success', 'Powodzenie');
					//powrot na SG z przerwaniem skryptu
					return res.redirect('/note/' + updatedMessage._id);
				});
			})
});
//usuniecie notatki
router.delete('/:id', function (req, res, next) {
	return res.send('delete note')
});

module.exports = router;

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) //is users logged in = authenticated
		return next();

	res.redirect('/');
}