var express = require('express');
var router = express.Router();

var User = require('../models/user');

//pokazanie formularza
router.get('/', isLoggedIn, function (req, res, next) {
	return res.render('user/edit');
});
//update usera
router.post('/', isLoggedIn, function (req, res, next) {

});


module.exports = router;

function isLoggedIn(req, res, next) {
	if (req.isAuthenticated()) //is users logged in = authenticated
		return next();

	res.redirect('/');
}