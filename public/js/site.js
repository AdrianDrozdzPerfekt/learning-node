$(document).ready(function(){
	$('.btn-memo-delete').click(function(){
		//pobieramy id notatki z atrybutu `data-target`
		var memoId = $(this).data('target');

		//jesli user pozwoli na usuniecie notatki
		if (confirm('Czy chcesz usunąć notatkę')) {
			//wysylamy zadanie do serwera 
			//metoda DELETE
			$.ajax({
				method: 'DELETE',
				url: '/delete-message/' + memoId,
			})
			.done(function(result) { alert(result) }) //sukces
			.fail(function(error) { alert(error) }) //blad
			.always(function() { location.href = '/' }); //zawsze
		}
	});
});