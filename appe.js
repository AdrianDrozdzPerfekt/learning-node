/**
	git fetch --all
	git reset --hard origin/master

	mongo
	use node-app
	db.dropDatabase()
	nodemon appe.js
*/

// set up
// require tools
var express 		= require('express');
var app				= express();
var port 			= process.env.PORT || 3000;
var session			= require('express-session');
var morgan			= require('morgan');
var bodyParser		= require('body-parser');
var cookieParser	= require('cookie-parser');
var mongoose		= require('mongoose');
var passport 		= require('passport');
var flash 			= require('flash');
var messageRoutes	= require('./routes/messages');
var userRoutes		= require('./routes/user');

var configDB 		= require('./config/mongodb.js');

//configuration
mongoose.connect(configDB.url);


//set up express application
app.use(morgan('combined')); //logging
app.use(cookieParser()); //auth
app.use(bodyParser.urlencoded({ extended: false })); //forms

app.use(express.static('public'));

app.set('view engine', 'ejs'); //templates

//session set up for passport
app.use(session({ 
	secret: 'ilovenode', 
	resave: true, 
	saveUninitialized: true,
})); //

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.use(function(req, res, next){
	res.locals.user = req.user;
	next();
})

require('./config/passport')(passport);
require('./routes/routes')(app, passport);

app.use('/note', messageRoutes);
app.use('/profile', userRoutes);

app.use(function(req, res, next){
	req.session.flash = [];
	next();
})

app.listen(port);
console.log('Let the magic begin at port ' + port);