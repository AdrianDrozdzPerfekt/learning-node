//load packages
var localStrategy 	= require('passport-local').Strategy;
var User			= require('../models/user');

module.exports = function(passport) {

	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	passport.deserializeUser(function(id, done) {
		User.findById(id, function(err, user) {
			done(err, user);
		})
	});

	passport.use('local-signup', new localStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
	},
	function (req, email, password, res) {
		
		var fullName = req.body.fullname;
		var rePassword = req.body.re_password;

		if (password !== rePassword) {
			return res(
				null, 
				false, 
				req.flash('danger', 'Hasła się nie zgadzają')
			);
		}

		User.findOne({ 'email': email }, function(err, user) {
			if (err) {
				return res(err);
			}

			if (user) {
				return res(
					null, 
					false, 
					req.flash('danger', 'Ten e-mail już jest zajęty')
				);
			} else {
				var newUser = new User();
				newUser.email = email;
				newUser.name = fullName;
				newUser.password = newUser.generateHash(password);

				newUser.save(function (err) {
					if (err) {
						throw err;
					}

					return res(null, newUser);
				});
			}
		});
	}));

	passport.use('local-login', new localStrategy({
		usernameField: 'email', //override username field
		passwordField: 'password', //override password field
		passReqToCallback: true //pass all request to callback
	},
	function (req, email, password, res) { //callback
		//find user with given email
		User.findOne({ 'email': email }, function (err, user) {
			if (err) //db error
				return res(err);
			//else
			if (!user) //no user found
				return res(null, false, req.flash('warning', 'Zły email lub hasło'));
			//else
			if (!user.validatePassword(password)) //bad password
				return res(null, false, req.flash('warning', 'Zły email lub hasło'));
			//else Hurra user can login
			return res(null, user);
		})
	}));

}